---
layout: markdown_page
title: "Category Direction - Runtime Application Self-Protection"
--- 
- TOC
{:toc}

## Description
Runtime Application Self-Protection (RASP) is a security technology that uses
runtime instrumentation to detect and block attacks by taking advantage of
information from inside the running software. RASP differs from
perimeter-based protections such as WAF, in that it is inside the application,
not sitting in front of it. RASP technology improves the security of software
by monitoring inputs, blocking those that could allow attacks, and protecting
the runtime environment from unwanted changes and tampering.

When a threat is detected, RASP can prevent exploitation and possibly take
other actions, including terminating a user's session, shutting the
application down, or alerting security personnel.

GitLab views RASP as complementary to the other Defend categories we offer,
designed to be used together as part of a "better together,"
defense-in-depth approach to defending applications.

### Goal

GitLab's goal is to be able to automatically add RASP to every application we
host and deploy, while simultaneously being able to identify and block attacks
with a minimal performance overhead.

Following our [Security Paradigm](https://about.gitlab.com/direction/secure/#security-paradigm)
and our [stage vision](https://about.gitlab.com/direction/defend/#inform-action-for-other-stages),
our goal is also to be able to provide actionable next steps to tracking
incidents, and fixing any underlying vulnerabilities that are being attacked.

Additionally, our goals include being able to offer security responses, that can
respond to attacks in different ways, such as terminating user session or
forcing step-up authentication, based on your risk posture and business
requirements.

### Roadmap
[Roadmap Board](https://gitlab.com/groups/gitlab-org/-/boards/1241267?label_name[]=devops%3A%3Adefend&label_name[]=runtime%20application%20security)

## What's Next & Why
Our next step is to [complete our MVC for RASP](https://gitlab.com/gitlab-org/gitlab/issues/33644).
This will be our first RASP offering and will be targeted to detect at least
one class of attack and prevent it for a specific use case.

## Competitive Landscape

* [Immunio (acquired by TrendMicro)](https://www.immun.io/)
* [Imperva](https://www.imperva.com/products/runtime-application-self-protection-rasp/)
* [Contrast Security](https://www.contrastsecurity.com/runtime-application-self-protection-rasp)
* [Hdiv](https://hdivsecurity.com/runtime-application-self-protection-rasp)

## Analyst Landscape

TODO

## Top Customer Success/Sales Issue(s)

There is no feature available for this category.

## Top Customer Issue(s)

The category is very new, so we still need to engage customers and get
feedback about their interests and priorities in this area.

## Top Vision Item(s)
TODO

## Upcoming Releases
TODO
