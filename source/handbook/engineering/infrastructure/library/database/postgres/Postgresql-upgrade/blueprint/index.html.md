---
layout: handbook-page-toc
title: "Blueprint: Postgresql Upgrade"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Resources

Epic: [Postgresql Upgrade](gl-infra/-/epics/106)

## Idea or problem statement

The goal of this document is to explain the alternatives that are feasible for executing the Postgresql database upgrade.

Upgrading the database will allow us to use new features that would make the work executed by developers and administrators easier. We can consider the following: 
* Table partitioning now with more functionalities and with better performance.
* CREATE INDEX can now use parallel processing while building a B-tree index, that would be faster.
* Quorum-based commit, that would ensure our transactions are committed in the specified nodes from the cluster.
* Among other features.

Initially, we would like to consider that version 11 is the best candidate, due to business requirements considering. These are the links to the list of features and bugs fixed:

[Postgresql v11.0](https://www.postgresql.org/docs/release/11.0/)

[Postgresql v11.1](https://www.postgresql.org/docs/release/11.1/)

[Postgresql v11.2](https://www.postgresql.org/docs/release/11.2/)

[Postgresql v11.3](https://www.postgresql.org/docs/release/11.3/)

[Postgresql v11.4](https://www.postgresql.org/docs/release/11.4/)

[Postgresql v11.5](https://www.postgresql.org/docs/release/11.5/)

[Postgresql v11.6](https://www.postgresql.org/docs/release/11.6/)

[Postgresql v11.7](https://www.postgresql.org/docs/release/11.7/)

Additionally, due to a business decision in version 13.0 from Gitlab, we would not support any longer Postgresql version 10.

Nevertheless, we are aware that the end of life support of the current version is relatively near:  November 11, 2021.

## Migration options:

The aim of this section is to describe the main options that are part of the migration. 

### 1 - Enable the checksum feature in the database. 

Enabling the checksum feature would allow the cluster to check for every data page being used to find out if the page is reliable or not. A reliable page is a page that has not been corrupted by writing data to the disk or reading back the data. This will add around approximately 2% of load overhead.

This step could require a long downtime in our current cluster to verify that all the pages are reliable. Setting up a new cluster with the checksum feature, and using logical replication to transfer all the data to this new cluster, could be the best solution to eliminate this downtime. Of course, this new cluster would be ready for a failover operation after receiving all the data. 

Respecting our values of avoiding data loss over availability this action is necessary for the database cluster.


### 2 - How to execute the migration : 

#### 2.1 - Upgrade with downtime : 

##### 2.1.1 - Executing pg_upgrade + enabling checksums

One option could be to execute pg_upgrade and activate the database checksums. The downtime could take hours, so we need to check in staging how long it would take to execute both steps to get a better estimate of time.

The positive sides:
- Technically it is a simple and straightforward procedure to implement and execute.
- The pre-requisites for this migration are minimum.

The negative side : 
- This approach would require a long downtime.

##### 2.1.2 - Executing pg_upgrade

One option would just be to execute pg_upgrade. The downtime would be short, so we need to check in staging how long it would take to execute in stagging to get a better estimate of time.

The positive sides:
- Technically it is a simple and straightforward procedure to implement and execute.
- The pre-requisites for this migration are minimum.

The negative side : 
- This approach would require downtime.


##### 2.2 - Without downtime or reduced downtime: 

Since is not possible to execute a stream replication between different major versions of Postgresql, and considering the goal of a minimum downtime, solutions that would use logical replication are needed.


##### 2.2.1-Execute a logical migration with Londiste.

Essentially, it is a project written in python to execute logical migrations and has not been updated recently.

Positive side:
- It was a reliable solution in the past.

Negative sides:
- Requires verification if it works properly in our environment due to old libraries and dependencies.
- It is based on triggers and adds load to the primary. That could generate a degraded situation during peak time. 
- Primary keys are required in all the tables.
- By default is compiled by “make” in the host. It is recommended to generate a package that would be an extra step.

##### 2.2.2-Execute a logical replication with Pglogical version 2.
It is an old version of a project that the latest version is under a commercial license, developed by 2nd quadrant.

Positive sides:
- It was a reliable solution in the past.
- It does not add load to the primary.

Negative sides:
- Implementation time.
- Extensive testing will be required since there are some limitations and restrictions that could impact our application: https://www.2ndquadrant.com/en/resources/pglogical/pglogical-docs/#limitations-and-restrictions 
- We would need to do our proper R&D in case of issues.
- During the setup, an extension load and Postgresql restart are required.


##### 2.2.3-Execute a logical replication with Egres.
Egres is a logical replication solution developed by Ongres.

Positive sides:
- Faster than Pglogical and Londiste.
- It Does not add load to the primary.
- Exports metrics to Prometheus for continuous monitoring.
- We would have support for the solution, and Ongres has hands-on experience.

Negative sides:
- We would need development cycles to get the solution ready for our environment.
- It could take longer to get this solution working properly.
- Not used by the community.


## Prerequisites :

Executing the [Enable external merge request diff storage](gl-infra/infrastructure/issues/7356) to a different data-source would be a pre-requisite. The result would release 33% of the database size and will reduce significantly the time cost and downtime of all the options above.
