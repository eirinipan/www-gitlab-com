---
layout: handbook-page-toc
title: "How to do UI Code Contributions"
---

#### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Designer's Guide to Contributing UI Code Changes

Requires HTML, CSS, Terminal (CLI), and Git Knowledge. Basic Ruby and JavaScript knowledge also encouraged.

### Start by Installing Your GDK
**GDK provides a local GitLab instance**

The GitLab Development Kit (GDK) allows you to test changes locally, on your workstation.

1. Learn how to prepare your workstation to run GDK: 
   - [gitlab-development-kit/prepare.md](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/master/doc/prepare.md)
1. Learn how to run GDK:
   - [gitlab-development-kit/set-up-gdk.md](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/master/doc/set-up-gdk.md)

### Choose a Code Editor and Start Making Small Changes
**Learn GitLab's UI templates**

Three kinds of files handle UI rendering. At its core, GitLab is a Ruby on Rails application. The Ruby on Rails application renders GitLab's front end with `.haml` files. HAML (HTML Abstraction Markup Language) is a Ruby-based HTML template system. It's easy to learn and it even closes HTML tags for you!

For Stylesheets, GitLab uses a CSS pre-processor called SASS. SASS (Syntactically Awesome Style Sheets) uses `.scss` files that handle all of the usual stuff CSS does, but with a bit more sophistication that helps us keep GitLab's CSS better organized.

Finally, for interactivity and client-side application logic, GitLab uses a framework called Vue.js. It's rare to have to change the `.vue` files unless you're changing a [pajamas](https://design.gitlab.com/) component, or creating a new one.

### Find Small Issues and Open Merge Requests for Your Changes

**Most fixes are a CSS change away**

If you can fix it in the browser inspector, you can probably fix it for real in the GitLab codebase. Find small UI issues and submit your changes via [merge requests (MRs)](https://docs.gitlab.com/ee/user/project/merge_requests/). Don't worry, you won't break anything, and a reviewer will always help you check your code before it ships.

### Terminal (CLI) Cheatsheet

| Action             | CLI command                                                                             |
| ------------------ | --------------------------------------------------------------------------------------- |
| Create new branch  | `git checkout -b your-feature-branch`                                                      |
| Rebase from master | `git checkout master`<br>`git pull`<br>`git checkout your-feature-branch`<br>`git rebase master` |
| Commit Changes     | `git add .`<br>`git commit -m "Your commit message..."`                                 |
| Create an MR       | `git push origin your-feature-branch`<br>(creates an upstream branch and a new MR)          |
| Push change to MR  | `git push --force-with-lease`                                                             |
| Heal your GDK      | `gdk reconfigure`                                                                         |
| Heal your Git      | `git prune`                                                                               |

Adapated from a [beautifully designed PDF](https://gitlab.com/gitlab-org/gitlab-design/-/blob/master/misc/infographics/How_to_Contribute_UI_Code_to_GitLab.pdf) created by [`@jj-ramirez`](https://gitlab.com/jj-ramirez) 😃